<?php
/**
 * @var stdClass $booking Booking row
 */

// Traitement des dates
$booking_start = DateTime::createFromFormat('Y-m-d H:i:s', $booking->date_arrivee);
$booking_end = DateTime::createFromFormat('Y-m-d H:i:s', $booking->date_depart);

?>
<div id="rf_mainDiv">
    <h2>_<?= __('Booking', 'reservation-facile') ?> #<?= $booking->id ?></h2>
    <div class="rf_previewBox">
        <div class="rf_previewHead"><?= __('Your contact details', 'reservation-facile') ?></div>
        <div class="rf_previewContent">
            <p><?= $booking->nom .' ' . $booking->prenom ?></p>
            <p><?= $booking->address ?></p>
            <p><?= $booking->code_postal . ' ' . $booking->ville ?></p>
            <p><?= $booking->pays ?></p>
            <p><?= __('Tel.', 'reservation-facile') . ' :' . $booking->telephone ?></p>
            <p><?= __('Email', 'reservation-facile') . ' :' . $booking->email ?></p>
            <p><?= __('Comment', 'reservation-facile') . ' :' . $booking->remarques ?></p>
        </div>
    </div>
    <div class="rf_previewBox">
        <div class="rf_previewHead"><?= __('Place', 'reservation-facile').': '.rf_removeslashes($booking->label).' - '.rf_removeslashes($booking->lieu) ?></div>
        <div class="rf_previewContent">
            <div class="rf_previewResume"><?= __('Booking date', 'reservation-facile').': ' . $booking_start->format('d.m.Y H:i') . ' ' . __('until', 'reservation-facile') . ' ' . $booking_end->format('d.m.Y H:i') ?></div>
            <div class="rf_previewRow">
                <div><?= __('Number of places', 'reservation-facile') ?></div>
                <div><?= (int)$booking->nb_de_place ?></div>
            </div>
        </div>
    </div>
    <div class="rf_previewBox">
        <div class="rf_previewContent">
            <p><?= __('You wish to cancel your booking ?', 'reservation-facile') ?></p>
            <p><a href="<?= home_url(); ?>/reservation-facile/?cancel=<?= $booking->cancel_token; ?>"><?= __('Click here to cancel your booking', 'reservation-facile') ?></a></p>
        </div>
    </div>
</div>