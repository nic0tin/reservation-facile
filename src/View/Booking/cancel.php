<?php
/**
 * @var string $error Errors
 * @var stdClass $booking Booking row
 */
global $post;
get_header();
?>
<article class="post">
    <div class="post-body">
    <?php
    if ($error) {
        echo '<p class="error">'. $error .'</p>';
    } else {
    ?>
    <p><?= __('Click here to confirm your booking cancellation.', 'reservation-facile'); ?></p>
    <form method="post">
        <!-- some inputs here ... -->
        <?php wp_nonce_field( 'rf_booking_cancellation', 'rf_booking_cancellation_nonce' ); ?>
        <input type="submit" name="rf_confirm_cancel" value="<?= __('Confirm', 'reservation-facile') ?>" />
    </form>
    <?php } ?>
    </div>
</article>
<?php
get_footer();
