<?php


namespace RfBooking\Controller;

class RfBookingController {
    protected static $request;

    protected $viewPath = '';
    protected $themeViewPath = '';

    public function __construct() {
        $this->viewPath = dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . 'View' . DIRECTORY_SEPARATOR;
        $this->themeViewPath = get_stylesheet_directory() . DIRECTORY_SEPARATOR . 'reservation-facile' . DIRECTORY_SEPARATOR . 'View' . DIRECTORY_SEPARATOR;

        $this->init();
    }

    protected function init() {
        add_action('rf_booking_saved', [$this, 'update_booking'], 10, 2);
        add_filter('rf_email', [$this, 'email'], 10, 2);
        add_action('init', function () {
            add_rewrite_endpoint('cancel', EP_PERMALINK);
        });

        add_action('template_redirect', [$this, 'cancel']);
    }

    public function update_booking($booking_form, $booking_id) {
        global $wpdb;

        $booking = $wpdb->query($wpdb->prepare("SELECT * FROM {$wpdb->prefix}rf_bookings WHERE id = %d", $booking_id));
        if (!$booking) {
            return;
        }

        $wpdb->update("{$wpdb->prefix}rf_bookings", ['cancel_token' => uniqid()], ['id' => $booking_id]);

        return;
    }

    public function email($content, $booking_id) {
        global $wpdb;

        $booking = $wpdb->get_row($wpdb->prepare("SELECT *, {$wpdb->prefix}rf_bookings.id AS id, {$wpdb->prefix}rf_spaces.id AS id_space FROM {$wpdb->prefix}rf_bookings LEFt JOIN {$wpdb->prefix}rf_spaces ON {$wpdb->prefix}rf_spaces.id = {$wpdb->prefix}rf_bookings.rf_idSpace WHERE {$wpdb->prefix}rf_bookings.id = %d", $booking_id));
        if (!$booking) {
            return;
        }

        return $this->render('Booking/Email/confirm', compact('booking'), false);
    }

   public function cancel() {
        global $wpdb, $wp_query;

        if (!isset($wp_query->query_vars['cancel']) || !is_404()) {
            return;
        }

        $booking_token = $wp_query->query_vars['cancel'];
        $error = null;
        if (!$booking_token) {
            $error = __("Missing booking number.", 'reservation-facile');
        }

        $booking = $wpdb->get_row($wpdb->prepare("SELECT *, {$wpdb->prefix}rf_bookings.id AS id, {$wpdb->prefix}rf_spaces.id AS id_space, tps_annulation_max_heure * 3600 AS timeout FROM {$wpdb->prefix}rf_bookings LEFt JOIN {$wpdb->prefix}rf_spaces ON {$wpdb->prefix}rf_spaces.id = {$wpdb->prefix}rf_bookings.rf_idSpace WHERE {$wpdb->prefix}rf_bookings.cancel_token = %s", $booking_token));
        if (!$booking) {
            $error = __("Unknown booking number.", 'reservation-facile');
        }

        if ($error) {
            echo $this->render('Booking/cancel', compact('error'), false);
            exit;
        }

        if ($booking->timeout > 0) {
            $timeleft = \DateTime::createFromFormat('Y-m-d H:i:s', $booking->date_depart)->diff((new \DateTime())->sub(new \DateInterval('PT'. (int)$booking->timeout .'S')));
            if ($timeleft->invert === 0) {
                $error = __("Canceling delay has expired", 'reservation-facile');
            }
        }

        // Already canceled?
        if ($booking->statut === 'canceled') {
            $error = __("Booking is already canceled.", 'reservation-facile');
        }

        if (
            isset($_POST['rf_confirm_cancel']) &&
            wp_verify_nonce( $_POST['rf_booking_cancellation_nonce'], 'rf_booking_cancellation' ) &&
            !$error
        ) {
            $wpdb->query($wpdb->prepare("UPDATE {$wpdb->prefix}rf_bookings SET statut = 'canceled' WHERE id = %d", $booking->id));
            rf_email([$booking->email, get_option('admin_email')], __("Booking cancellation", 'reservation-facile'), $this->render('Booking/Email/cancellation', compact('booking'), false));
            $error = __('Booking canceled', 'reservation-facile');
        }

        echo $this->render('Booking/cancel', compact('error', 'booking'));
        exit;
    }

    protected function render($view, $vars = [], $output = true) {
        extract($vars);

        if (!$output) {
            ob_start();
        }

        include $this->_viewPath($view);

        if (!$output) {
            return ob_get_clean();
        }
    }

    private function _viewPath($view) {
        if (file_exists($this->themeViewPath . DIRECTORY_SEPARATOR . $view . '.php')) {
            return $this->themeViewPath . DIRECTORY_SEPARATOR . $view . '.php';
        }

        return $this->viewPath . DIRECTORY_SEPARATOR . $view . '.php';
    }
}